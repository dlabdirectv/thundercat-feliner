#!/bin/bash

# install.sh: Thundercat scaffolding app

# Variables

echo "Scaffolding your thundercat app...hoooooo!"

echo $1

#validate parameter

echo "What is app name?"

read appname

mkdir $appname

cd $appname

mkdir docs routes views public

mkdir public/img public/js public/stylesheets

touch app.js config.js README.md package.json .gitignore

touch routes/index.js

# update .gitignore
echo '_Store
lib-cov
*.seed
*.log
*.csv
*.dat
*.out
*.pid
*.swp
*.swo
node_modules
node_modules/
node_modules/*' > .gitignore

# update config.js
echo "
var config = exports;

var currentEnv = process.env.NODE_ENV || 'local';

config.appName = 'kids-app';

config.env = {
    production: false,
    staging: false,
    test: false,
    development: false
};

config.env[currentEnv] = true;

config.log = {
    path: __dirname + ('/var/log/app_' + currentEnv + '.log')
};

config.server = {
    port: 3000,
    ip: '127.0.0.1'
};

if (currentEnv !== 'production' && currentEnv !== 'staging') {
    config.enableTests = true;
    config.server.ip = '0.0.0.0';
}

config.template_engine = {
    host: 'localhost',
    port: 3001,
    path: '/templates/'
};" > config.js

# update package.json
echo '{
  "name": "application-name",
  "version": "0.0.1",
  "contributors": [
    {
      "name": "Rusty Calder",
      "email": "rcalder@directv.com"
    },
    {
      "name": "Isreal Shortman",
      "email": "ishortman@directv.com"
    }
  ],
  "private": true,
  "scripts": {
    "start": "node app"
  },
  "dependencies": {
    "express": "3.0.0rc5",
    "underscore": "1.4.2",
    "Thundercat-tygra": "http://repo.cloudster.tv/npm/Thundercat-tygra-1.0.3.tgz"
  },
  "engines": {
    "node": "0.8.x",
    "npm": "1.1.x"
  }
}' > package.json

# update app.js
echo "/*
 * Module dependencies
 */

var express = require('express')
    , http = require('http')
    , routes = require('./routes')
    , path = require('path') 
    , tygra = require('Thundercat-tygra')

var app = express();

// config data
var config = require('./config');

// init template engine (host, port, path) - config
tygra.init(config.template_engine.host, config.template_engine.port, config.template_engine.path);

app.configure(function(){
  
  // port set to 3000
  app.set('port', process.env.PORT || config.server.port);

  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.compress());
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('dl@b5'));
  app.use(express.session());
  app.use(app.router);

  // static routes
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
});

app.configure('staging', function(){
  app.use(express.errorHandler({ dumpExceptions: true })); 
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

app.get('/', routes.index);

// start node server
var server = http.createServer(app).listen(app.get('port'), function(){
    console.log('Thundercat app on port ' + app.get('port'));
});" > app.js

echo "/*
	router page
*/

var tygra = require('Thundercat-tygra');

exports.index = function(req, res){
  tygra.render('demo', { name: 'App', title: 'Thundercat testing' }, res, req);
};" > routes/index.js

echo ""

echo "Installing dependencies"

npm install

echo ""

echo "Thundercat app created...hoooooo!"

echo ""

echo "run the thundercat app:
     $ node app"

