#!/bin/bash

# install.sh: Installation script

# Variables
NODEVER=0.8.8

# checking OS
LINUX=0
MAC=0
PKGMGR=0

haveProg() {
	[ -x "$(which $1)" ]
}

echo ""
echo "Hang on...get ready!"
echo "We're gonna install thundercat framework."
echo ""

# checking baseline dependencies
CURLFILE=$(which curl)
NODEFILE=$(which node)
NGINX=$(which nginx)
MACPORTS=$(which port)
CLANG=$(which clang)
THUNDERCAT=$(which thundercat)

# check for macports
# check for command line tools
# check for nginx

sysver=`sw_vers -productVersion | cut -c 1-4`

echo "Your mac version is: " $sysver

if [ -z "$CURLFILE" ]; then
	echo "Wait a sec, you need to install curl before we proceed!"
	exit 1
fi

if haveProg clang; then
	echo "You have unix command line tools installed, good job!"
else
	echo "Looks like you need the XCode CLI Tools for homebrew, chap. Learn about
where to install them at the thundercat docs: https://bitbucket.org/dlabdirectv/thundercat-feliner/wiki/Manual%20Installation"
	exit 1
fi

#if [ -z "$MACPORTS" ]; then
#	if [ $sysver = 10.8 ]; then
#		echo "Downloading macports for Mountian Lion Mac"
#		curl -O https://distfiles.macports.org/MacPorts/MacPorts-2.1.3-10.8-MountainLion.pkg
#		echo "Macports downloaded, starting installer"
#		sudo installer -pkg MacPorts-2.1.3-10.8-MountainLion.pkg -target /
#	elif [ $sysver = 10.7 ]; then
#		echo "Downloading macports for Lion Mac"
#		curl -O https://distfiles.macports.org/MacPorts/MacPorts-2.1.3-10.7-Lion.pkg
#		echo "Macports downloaded, starting installer"
#		sudo installer -pkg MacPorts-2.1.3-10.7-Lion.pkg -target /
#	else
#		echo "This system is too old"
#		exit 1
#	fi
#elif [ "$MACPORTS" ]; then
#	echo "You have macports installed, good job!"
#fi

if [ "$MACPORTS" -eq 0 ]; then
	echo "Wait, you need to install macports.  Please refer here:"
	exit 1
else
	echo "You have macports installed, good job!"
fi

if [ -z "$NGINX" ]; then
	# download and install nginx from macports
	# sudo port install nginx +ssl
	# echo configuration
	echo "Installing nginx via macports"
	sudo port install nginx +ssl
	echo "Nginx installed"
elif [ "$NGINX" ]; then
	echo "You have nginx installed, good job!"
fi

# check if node is available
if [ -z "$NODEFILE" ]; then
	echo "Downloading Node.js for Mac."
    curl -O http://nodejs.org/dist/v$NODEVER/node-v$NODEVER.pkg
    echo "Node.js downloaded, starting installer."
    sudo installer -pkg node-v$NODEVER.pkg -target /
elif [ "$NODEFILE" ]; then
	echo "You have nodejs installed, good job!"
fi

# install thundercat script
if [ -z "$THUNDERCAT" ]; then
	echo "Downloading Thundercat for Mac."
	# https://bitbucket.org/dlabdirectv/thundercat-feliner/raw/master/installer.sh
	curl -O https://bitbucket.org/dlabdirectv/thundercat-feliner/raw/master/thundercat.sh
	echo "Thundercat downloaded, starting installation."
	sudo rm /usr/local/bin/thundercat
	sudo install thundercat.sh /usr/local/bin/thundercat
	rm thundercat.sh
else
	echo "You have thundercat installed, good job!"
fi

echo ""


echo "                                         .......                                              "
echo "                                ..;clodddddddddddxdol:'.                                      "
echo "                            .;lddo:;'...............':oxxo:.                                  "
echo "                         .cddc,.......................  .,lxxl.                               "
echo "                      .:dd;............ ......''''''........'ckkc.                            "
echo "                    .ld:. .....................'''''.......''..,oko.                          "
echo "                  .lx,  .........................'..... .........;oOo.                        "
echo "                 ;xc.................................',:ldk00KKKOx,'dO:                       "
echo "               .lx.  .........................,:oxO0KXWMMMMMW0d:ckK0:c0o.                     "
echo "              .do.   ..................';lx0XNMMMMWKkxxxxddoclkXWMMMNkoKx.                    "
echo "             .dd.   ...............'cdxodXMMMMMMMMMMMMWNXKKXWMMMMMMMMWOdKx.                   "
echo "             ok.    ........... .cO0d;cOWMMMMMMMMMMNNWMMMMMMMMMMMMMMMMNdcKd.                  "
echo "            ,O'      ..........lK0c.;OWMMMMMMMMMMMNOdxkk0XWMMMMMMMMMMMMNcoKc                  "
echo "           .Od    ....'..   'oOOl:oKWMMMMMMMMMMMMMMMMMNXKkdxKWMMMMMMMMMWOl0k.                 "
echo "           ,O'    .......'oKNXOOXWMMMMMMMMMMMMMMMMMMMMMMMMWXkxXWMMMMMMMMNdxX:                 "
echo "           ck.    ....:xKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKokWMMMMMMMWdlKo.                "
echo "           d0'.   .;xXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWd'kWMMMMMMWx:0x.                "
echo "           xK;  'oKWMMMMMMMMMMMMMMWNK0OkO0KNWMMMMMMMMMMMMMMMMMK;oNMMMMMMWd:Kx.                "
echo "           l0.,0NMMMMMMMMMMMMMN0dcccc:;,;;clxXMMMMMMMMMMMMMMMM0,dWMMMMMMNlcKd.                "
echo "           ;0,'dKWMMMMMMMMMN0d;.    .';ccccc:kWMMMMMMMMMMMMMMWocXMMMMMMW0,oKl.                "
echo "           .0O..;0MMMMMMMNx,.........   ...'.oNMMMMMMMMMMMMMWx.,x0KNWMMNl'OO;                 "
echo "            :K: .cNMMMMMNx'..............    oNMMMMMMMMMMMWXd'    .,oKWO,oKo.                 "
echo "            .x0,..:dKWMMNl.         ....... 'KMMMMMMMMWWXOdc,.    ...,xcc0k,                  "
echo "             .O0,...,xNMWO,.               .xWMMMMMWKd:'.....     .....;0O:.                  "
echo "              .OO'...':xXWO:...           .xWMMMMNx:..               .;00:.                   "
echo "               .x0; ....;o0Kx;...        ;0WMMMMXc   ....      ......:0O:.                    "
echo "                .l0d.......,,..,,..  .,o0WMMMMMWx.  'lc...    .';::ckKx,.                     "
echo "                  'x0l...''.. .ckKKKXNWMMMMMMMMMN00KKo. ...   .;clx0Oc.                       "
echo "                    ,k0o'..''....';xWMMMMMMMMMMMWNOc.....'.   'cx0Ol'.                        "
echo "                      'dOOo,.......oXxldOXNWWNKd,.  .'..... .cO0kc'.                          "
echo "                        .;x00d:.. .'.    .....     .....,:ok0kl,.                             "
echo "                           .;ok00xdc'..         ..';cox00Oxl,..                               "
echo "                               .,:oxkO0K0OkkkO00K00Okdl:'..                                   "
echo "                                    ...',,;::;;,,'...                                         "


echo ""
echo "We are Thundercats Upholding Justice, Truth, Honor and Loyalty."
echo "Thundercats HOOOOOO!"
